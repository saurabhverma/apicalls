package com.example.clicklabs124.apicalls.util;

import android.app.Application;

import com.example.clicklabs124.apicalls.interfaces.WebServices;

import retrofit.RestAdapter;

public class GlobalClass extends Application {

    final String domain = "http://jsonplaceholder.typicode.com";
    WebServices webServices;

    public WebServices getWebServices() {
        webServices = new RestAdapter.Builder()
                .setEndpoint(domain).build().create(WebServices.class);
        return webServices;
    }


}
