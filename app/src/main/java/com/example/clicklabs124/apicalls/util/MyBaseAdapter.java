package com.example.clicklabs124.apicalls.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.clicklabs124.apicalls.R;
import com.example.clicklabs124.apicalls.entity.User;

import java.util.ArrayList;


public class MyBaseAdapter extends BaseAdapter {
    ArrayList<User> studentList = new ArrayList<User>();

    LayoutInflater inflater;
    Context context;

    public MyBaseAdapter(Context context, ArrayList<User> myList) {
        this.studentList = myList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return studentList.size();
    }

    @Override
    public User getItem(int position) {
        return studentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listview_each_item, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        User studentData = getItem(position);

        //mViewHolder.roll.setText(String.valueOf(studentData.getId()));
        mViewHolder.name.setText(studentData.getName());
        return convertView;
    }

    private class MyViewHolder {
        TextView roll, name;

        public MyViewHolder(View item) {
            //roll = (TextView) item.findViewById(R.id.userId);
            name = (TextView) item.findViewById(R.id.user_name);
        }
    }
}
