package com.example.clicklabs124.apicalls;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clicklabs124.apicalls.entity.User;
import com.example.clicklabs124.apicalls.interfaces.WebServices;
import com.example.clicklabs124.apicalls.util.GlobalClass;
import com.example.clicklabs124.apicalls.util.MyBaseAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends Activity {

    WebServices webServices;
    MyBaseAdapter adapter;
    ListView listView;
    int selectedListItem;
    TextView id, userName, uName, email, phone, website, address, company;
    Button viewButton;
    ArrayList<User> userData = new ArrayList<User>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GlobalClass globalClass = (GlobalClass) getApplicationContext();
        webServices = globalClass.getWebServices();
        listView = (ListView) findViewById(R.id.userList);
        adapter = new MyBaseAdapter(this, userData);
        listView.setAdapter(adapter);
        getUserData();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = userData.get(position);
                selectedListItem = position;
                showUser(user);
            }
        });
    }


    public void getUserData() {
        try {
            webServices.getUser(new Callback<List<User>>() {
                @Override
                public void success(List<User> users, Response response) {
                    User user;
                    for (int i = 0; i < users.size(); i++) {
                        user = users.get(i);
                        userData.add(user);
                    }
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (NullPointerException e) {
            e.fillInStackTrace();
        }
    }

    public void showUser(User user) {
        id = (TextView) findViewById(R.id.id);
        userName = (TextView) findViewById(R.id.name);
        uName = (TextView) findViewById(R.id.username);
        email = (TextView) findViewById(R.id.email);
        phone = (TextView) findViewById(R.id.phone);
        website = (TextView) findViewById(R.id.website);
        address = (TextView) findViewById(R.id.address);
        company = (TextView) findViewById(R.id.company);
        viewButton = (Button) findViewById(R.id.viewButton);
        id.setText(String.valueOf(user.getId()));
        userName.setText(user.getName());
        uName.setText(user.getUsername());
        email.setText(user.getEmail());
        phone.setText(user.getPhone());
        website.setText(user.getWebsite());
        address.setText(user.address.street + " " + user.address.suite + " " + user.address.city + " - " + user.address.zipcode);
        company.setText(user.company.name + " " + user.company.catchPhrase + " " + user.company.bs);
        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                User user = userData.get(selectedListItem);
                Intent intent = new Intent(MainActivity.this, UserPost.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
            }
        });
    }
}
