package com.example.clicklabs124.apicalls;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clicklabs124.apicalls.entity.Post;
import com.example.clicklabs124.apicalls.interfaces.WebServices;
import com.example.clicklabs124.apicalls.util.GlobalClass;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class UserPost extends Activity {

    WebServices webServices;
    TextView userPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_post);
        GlobalClass globalClass = (GlobalClass) getApplicationContext();
        webServices = globalClass.getWebServices();
        userPosts = (TextView) findViewById(R.id.userPost);
        int userId = getIntent().getIntExtra("userId", 0);
        getPostsByUser(userId);
    }

    public void getPostsByUser(int userId) {
        try {
            webServices.getPostOfUser(userId, new Callback<List<Post>>() {
                @Override
                public void success(List<Post> posts, Response response) {
                    try {
                        Post post;
                        String text = "";
                        for (int i = 0; i < posts.size(); i++) {
                            post = posts.get(i);
                            text += "Post Id : " + post.getId() + "\nTitle : " + post.getTitle() + "\nPost : " + post.getBody() + "\n\n";
                        }
                        userPosts.setText(text);

                    } catch (NullPointerException e) {
                        Toast.makeText(UserPost.this, "ID not Found", Toast.LENGTH_SHORT).show();
                    }
                }

                public void failure(RetrofitError error) {
                    Toast.makeText(UserPost.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (NullPointerException e) {
            e.fillInStackTrace();
        }
    }
}
