package com.example.clicklabs124.apicalls.entity;

public class User {
    int id;
    String name, username, email, phone, website;
    public Address address;
    public Company company;

    public static class Address {
        public String street, suite, city, zipcode;

        public class geo {
            String lat, lng;
        }
    }

    public static class Company {
        public String name, catchPhrase, bs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }


    public String getEmail() {
        return email;
    }
    

    public String getPhone() {
        return phone;
    }


    public String getWebsite() {
        return website;
    }
}
