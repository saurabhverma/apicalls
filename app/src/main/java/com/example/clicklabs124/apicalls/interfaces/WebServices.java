package com.example.clicklabs124.apicalls.interfaces;

import com.example.clicklabs124.apicalls.entity.Post;
import com.example.clicklabs124.apicalls.entity.User;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;


public interface WebServices {
    @GET("/users")
    void getUser(Callback<List<User>> callback);

    @GET("/posts")
    void getPostOfUser(@Query("userId") int param, Callback<List<Post>> callback);
}
