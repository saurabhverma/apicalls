package com.example.clicklabs124.apicalls.entity;

public class Post {
    int userId;
    int id;
    String title;
    String body;

    public int getUserId() {
        return userId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }
}

